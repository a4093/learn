# build docker image
* git clone https://github.com/mineplexio/Pool-Script.git
* cd Pool-Script
* rm -r js-rpcapi
* rm -rf .git*
* git submodule add https://github.com/mineplexio/js-rpcapi.git
* cd ..
* docker build --tag pool-script
* 
