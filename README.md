## smart contracts
* [DeFI](https://www.youtube.com/watch?v=pWGLtjG-F5c&list=PLjrTIwaNiTwn39tg3sR_bPBWGHoznv47D)
## tezos
* https://wiki.tezosagora.org/
* https://tezos.gitlab.io/
* https://wiki.tezosagora.org/build/clients/block-explorers
* https://gitlab.com/tezos
* indexers
    * https://github.com/blockwatch-cc
* libs
    * [The SmartPy language is available through a Python library for building and analyzing Tezos smart contracts.](https://smartpy.io)
    * [mpapi](https://libraries.io/npm/mineplex-rpcapi)
    * https://github.com/mineplexio/js-rpcapi

## mineplex
* https://github.com/mineplexio
* https://us02web.zoom.us/rec/share/gZpiU_oPt3HsHmkeJpNZTB0oZ_vSH81gsP6WGk6BeB5nT7900nkl5nezK9bV7Q6n.E61tiG4VlizupPep
* https://github.com/StasMatveev/instruction-mineplex/blob/main/full_guide.rst
* https://github.com/mineplexio/Plexus-Pool/blob/mineplex-beta-protocol/docs/introduction/howtorun.rst


## node 
* import/export
```
mineplex-node snapshot export --block head  /head.full --data-dir /mineplex-mainnet
mineplex-node snapshot import /head.full --block <block id> --data-dir /mineplex-mainnet
mineplex-node reconstruct  --data-dir /mineplex-mainnet
```
