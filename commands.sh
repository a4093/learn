#!/bin/bash
function list {
	echo "
known_contracts
node_status
baker_rights <cycle>
mine_balance <wallet>
add_address <alias> <address>
import_secret_key <alias> <secret key>
import_public_key <alias> <public key>
delegate <for wallet> <to wallet>
logs_baker
logs_endorser
endorsing_rights <cycle>
delegates
"
}

function logs_baker {
	journalctl -f -u mineplex-baker.service
}

function logs_endorser {
	journalctl -f -u mineplex-endorser.service
}

function constats {
        pushd ~/mineplex.blockchain
		block=`./mineplex-client rpc get /chains/main/blocks  | awk -F '"' '{print $2}'`
		./mineplex-client rpc get /chains/main/blocks/$block/context/constants
        popd
}

function known_contracts {
	pushd ~/mineplex.blockchain
		./mineplex-client list known contracts
	popd
}

function node_status {
        w=$1
        w="${w:-mpXXXXXXXXXXXXXXXXXX}"
	b=$2
	b="${b:-head}"
	pushd ~/mineplex.blockchain
                ./mineplex-client rpc get /chains/main/blocks/$b/context/delegates/$w/
        popd
}

function cycle_levels {
        b=$2
        b="${b:-head}"
	pushd ~/mineplex.blockchain
		./mineplex-client rpc get /chains/main/blocks/head/helpers/levels_in_current_cycle
	popd
}


function baker_rights {
	w=$2
	w="${w:-mpXXXXXXXXXXXXXXXXXX}"
	pushd ~/mineplex.blockchain
		./mineplex-client -endpoint http://127.0.0.1:8732/ rpc get /chains/main/blocks/head/helpers/baking_rights?cycle=$1\&delegate=$w\&max_priority=10

	popd
}

function mine_balance {
	pushd ~/mineplex.blockchain
		./mineplex-client get mine_balance for $1
	popd
}


function balance {
        pushd ~/mineplex.blockchain
                ./mineplex-client get balance for $1
        popd
}

function add_address {
#name address
	pushd ~/mineplex.blockchain
		./mineplex-client add address $1 $2
	popd
}

function import_secret_key {
 #name key
	pushd ~/mineplex.blockchain
                ./mineplex-client import secret key $1 $2
        popd
}

function import_public_key {
 #name key
        pushd ~/mineplex.blockchain
                ./mineplex-client import public key $1 $2
        popd
}

function delegate {
	 pushd ~/mineplex.blockchain
			./mineplex-client -endpoint http://127.0.0.1:8732/ set delegate for $1 to $2
 	 popd
}

function delegates {
	pushd ~/mineplex.blockchain
		./mineplex-client rpc get /chains/main/blocks/head/context/delegates?active=true
	popd
}

function endorsing_rights {
        w=$2
        w="${w:-mpXXXXXXXXXXXXXXXXXX}"
        pushd ~/mineplex.blockchain

	./mineplex-client -endpoint http://127.0.0.1:8732/ rpc get /chains/main/blocks/head/helpers/endorsing_rights?delegate=$w\&cycle=$1
        popd
}
