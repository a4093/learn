#monitoring 
* grafana 
    * https://grafana.com/
    * docker
        * grafana/grafana:latest
* process metrics
    * https://grafana.com/grafana/dashboards/249
    * docker
        * ncabatoff/process-exporter
* node-explorer
    * https://prometheus.io/docs/guides/node-exporter/
    * https://grafana.com/grafana/dashboards/1860
    * docker:
        * quay.io/prometheus/node-exporter:latest
* prometheus  
    * docker:
        * prom/prometheus:latest
        * prom/pushgateway:latest
#data flow
* nifi
#db
* mongo
* elasticksearch
